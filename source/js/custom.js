$(document).ready(function () {
	$("#rtd-search-form.wy-form > input:nth-child(3)").after($('<button type="submit">Submit</button>'));
	$("form#rtd-search-form.wy-form input:nth-child(1)").attr('placeholder', 'Search the Koha manual');
	$("form#rtd-search-form.wy-form input:nth-child(1)").css({"border-radius": "unset",
		"border-top-left-radius": "50px", "border-bottom-left-radius": "50px"});
});
